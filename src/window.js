/* window.js
 *
 * Copyright 2021 LucasPB710
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

imports.gi.versions.Gtk = '4.0';
const { GObject, Gtk } = imports.gi;
const gerador = imports.gerador;
const GLib = imports.gi.GLib;
const Gio  = imports.gi.Gio;

var GeradorjsWindow = GObject.registerClass({
    GTypeName: 'GeradorjsWindow',
    Template: 'resource:///com/lucaspb/GeradorJS/window.ui',
    InternalChildren: ['back',
                      'back2',
                      'back3',
                      'msg_txt',
                      // 'mensagem_traduzida',
                      'txt_arq',
                      'txt_botao',
                      'txt_page_bt',
                      'traduzir',
                      'palavra',
                      'palavra_page_bt',
                      'palavra_trad',
                      'silabas_page_bt',
                      'silabas_auto',
                      // 'silabas_arq',
                      // 'silabas_arq_enviar',
                      'dicionario_page_bt',
                      'stack',
                     ]
}, class GeradorjsWindow extends Gtk.ApplicationWindow {
    _init(application) {
        Gtk.Settings.get_default().gtk_application_prefer_dark_theme = true;
        super._init({ application });

        this._silabas_page_bt.connect('clicked',this.silabas.bind(this));//Muda para a pagina onde você fornece o arquivo de silabas
        this._silabas_auto.connect('clicked', this.adicionarsilabas.bind(this));//Chama gerador.adicionarsilabas
        this._txt_page_bt.connect('clicked',this.txtselec.bind(this));//Muda para a pagina onde voce fornece um arquivo txt para tradução
        this._txt_botao.connect('clicked', this.txtbot.bind(this));//Traduz o arquivo txt fornecido
        this._palavra_page_bt.connect('clicked', this.palselec.bind(this));//Vai para a pagina que traduz palavra por palavra
        this._back.connect('clicked', this.back.bind(this));//Volta para a pagina inicial
        this._back2.connect('clicked', this.back.bind(this));//Volta para a pagina inicial
        this._back3.connect('clicked', this.back.bind(this));//Volta para a pagina inicial
        this._traduzir.connect('clicked',this.traduzir.bind(this));//Traduz a palavra fornecida

    }

    silabas(){
      this._stack.visible_child_name = 'silabas_page' ;
    }

    adicionarsilabas(){
      gerador.adicionarsilabas(GLib.get_user_config_dir()+"/silabas.txt");
    }

    txtselec(){
      this._stack.visible_child_name = 'txt_page';
    }

    txtbot(){
      const PERMISSIONS_MODE = 0o744;
      // this._mensagem_traduzida.set_text("",0);
      // gerador.palavras_txt = [];
      // gerador.palsG = [];
      var arq = this._txt_arq.get_text();
      var msg_trad = "\nARQUIVO TRADUZIDO: \n";
      gerador.gerar_2(arq);
      for(var i = 0; i<gerador._linhas.length;i++){
        msg_trad+=gerador._linhas[i]+'\n';
      }
      // this._msg_txt.set_label(msg_trad);
      gerador._linhas = [];

      var dicio = "DICIONARIO:\n";
      for(var i = 0; i<gerador.palavras_txt.length; i++){
        dicio += gerador.palavras_txt[i] + " = " + gerador.palsG[i]+"\n";
      }
      log(gerador.palavras_txt);

      // this._mensagem_traduzida.set_text(dicio + msg_trad,msg_trad.length + dicio.length);
      let path = GLib.build_filenamev([GLib.get_home_dir(), "TRADUZIDO.txt"]);
      let file = Gio.File.new_for_path(path);
      if (GLib.mkdir_with_parents(file.get_parent().get_path(), PERMISSIONS_MODE) === 0) {
           let [success, tag] = file.replace_contents(msg_trad, null, false, Gio.FileCreateFlags.REPLACE_DESTINATION, null);
      }

      let path2 = GLib.build_filenamev([GLib.get_home_dir(), "DICIONARIO.txt"]);
      let file2 = Gio.File.new_for_path(path2);
      if (GLib.mkdir_with_parents(file2.get_parent().get_path(), PERMISSIONS_MODE) === 0) {
           let [success, tag] = file2.replace_contents(dicio, null, false, Gio.FileCreateFlags.REPLACE_DESTINATION, null);
      }
    }

    palselec(){
      this._stack.visible_child_name = 'palavra_page';
    }

    get_msg(){
        var msg = this._lel.get_text();
        log(msg);
        gerador.gerar(msg);
    }

    back(){
      this._stack.visible_child_name = 'Inicio';
    }

    traduzir(){
      var msg = this._palavra.get_text();
      gerador.gerar(msg);
      this._palavra_trad.set_text("palavra traduzida: "+ gerador.palG);
      gerador.palG="";
    }
      // jeej() {
      //   log('lel');
      // }
});

