/*TODO:
[ ] - Melhorar codigo, ta uma merda de ler
*/


const GLib = imports.gi.GLib;
let silabas = []; //array que armazena todas as silabas
var palG = ""; //Palavra individual gerada pelo programa (usada na funcao gerar())
var palsG = [];//array de palavras geradas (usada em gerar_2())
var _linhas = [];//array com todas as linhas
var palavras_txt = [""]; //array com todas as palavras do arquivo, porem sem palavras repetidas


function gerar_2(input) { //Gerar baseado num txt
/*
  Coloca tudo vazio para garantir que quando a pessoa for gerar um
  novo arquivo traduzido ela nao armazene coisas do ultimo arquivo
  usado.
*/
  palsG = [];
  _linhas = [];
  palavras = [],palavras_txt = [""];;

  //Abre o arquivo que é passado como argumento da função
  var file = String(GLib.file_get_contents(input)[1]);

  linhas_txt = file.split(/[\n ]+/);//Todas as palavras do arquivo, contem palavras repetidas
  linhas_de_vdd = file.split(/[\n]/);//linhas do arquivo

  // var pal = linhas_txt; nem é usado

  for(var i =0; i<linhas_txt.length; i++){
    var contem = 0;
    for(var j = 0; j<palavras_txt.length; j++){
      if(linhas_txt[i] === palavras_txt[j])
        contem = contem+1;
        //Verifica se a palavra em linhas_txt ja existe na array palavras_txt
    }

    if(contem === 0 && linhas_txt[i] != null){
      palavras_txt.push(linhas_txt[i]);
      //Adiciona palavras à essa array, garantindo que nao tenham palavras repetidas
    }
  }

  //Remove o primeiro elemento da array que, por algum motivo, sempre é um caractere vazio
  palavras_txt.splice(0,1);

  // log(palavras_txt);
  // var linhas = [];

  for(var i = 0; i<palavras_txt.length; i++){
    //gera uma palavra para cada palavra do arquivo
    gerar(palavras_txt[i]);
    palsG.push(palG);
    // log(palG);
    palG="";
  }

  for(var i = 0; i<linhas_de_vdd.length; i++){
    var linha = "";
    //Sempre reseta essa variavel, garantindo que ela nao contenha informações sobre as linhas geradas anteriormente

    var _linhas_de_vdd = linhas_de_vdd[i].split(' ');
    //Separa a linha em palavras

    for(var j = 0; j<_linhas_de_vdd.length;j++){
      for(var k =0;k<palavras_txt.length;k++){
        //Verifica se uma palavra de linhas é uma palavra do arquivo
        //se for, adiciona uma palavra gerada correspondente a essa
        //palavra na nova linha, que contem apenas palavras geradas
        if(_linhas_de_vdd[j] == palavras_txt[k])
          linha += palsG[k]+" ";
      }
    }

    _linhas.push(linha+"\n");
  }
  // log(_linhas);
}


function gerar(input){
  var tmnP = 0;//Tamanho da palavra, influencia na quantidade de silabas

  if(input.length > 6){
    if(input.length%2==0)
          tmnP = input.length/2;
      else
          tmnP = (input.length+1)/2;
  }

  else
    tmnP = input.length;

  var tmnG = Math.floor(Math.random()*tmnP+1);

  while(tmnG<2)
    tmnG = Math.floor(Math.random()*tmnP+1);

  for(var i = 0; i<tmnG; i++){
    var _rand = Math.floor(Math.random()*silabas.length+1);
    var rand_sil = silabas[_rand];
    palG += rand_sil;
  }
}

function adicionarsilabas(input){

  var file = String(GLib.file_get_contents(input)[1]); //DESCOMENTAR ISSO QUANDO OS TESTES TERMINAREM
  // var file = String(GLib.file_get_contents("/home/eu/silabas.txt")[1])

  file = file.replace(/\u000d/g,'');
  linhas = file.split('\n'); //Separa o arquivo em linhas, cada linha é uma silaba

  for(var i = 0; i<linhas.length; i++){
    silabas[i] = linhas[i];
  }
}


