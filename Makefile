all:
	flatpak-builder --repo=build-repo build com.lucaspb.GeradorJS.json
	cd build/files/bin
	flatpak build-bundle build-repo GeradorJS.flatpak com.lucaspb.GeradorJS
	cd ../../..
	mkdir -p ~/.var/app/com.lucaspb.GeradorJS/config
	cp silabas.txt ~/.var/app/com.lucaspb.GeradorJS/config/
clean:
	rm -rf ./build-repo build GeradorJS.flatpak
