# Sobre

Esse programa gera palavras aleatorias baseadas numa lista de silabas do português e na palavra fornecida ao programa, gerando uma "Lingua nova", por exemplo, se eu quero traduzir "balde" para a nova lingua, o programa retornará algo aleatorio, baseado nas silabas do português, como "calete" por exemplo.

OBS.: nem todas as silabas estão disponiveis no arquivo silabas.txt, porem sinta-se à vontade para expandir esse arquivo de silabas com novas silabas, para isso, caso você nao queira checar individualmente cada silaba para saber se ja existe ou não (silabas repetidas nao são aceitas), recomendo dar uma olhada [nesse repositorio](https://github.com/LucasPB710/GeradorDeLingua) para adicionar silabas novas.

## Como compilar

#### Com GNOME Builder

Teoricamente é bem direto, caso você esteja usando o gnome builder, boa parte da compilação pode ser feita com ele apenas clicando na seta em cima

------------------------------

#### Sem GNOME Builder

Caso você nao possua o gnome builder ou nao quer compilar usando ele, siga os seguintes passos:

 - 1. criar arquivo .flatpak
 
 ```
 make
 ```

 - 2. Instalar arquivo .flatpak
 
 ```
 flatpak install GeradorJS.flatpak
 
 ou
 
 flatpak install --user GeradorJS.flatpak
 ```
